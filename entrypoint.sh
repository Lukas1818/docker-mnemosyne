#!/bin/bash
set -e
set -u

data_dir=/data

test -s "$data_dir/config.db" || timeout --kill-after=2m --signal=SIGINT 30s mnemosyne "--datadir=$data_dir" --web-server || test $? == 124

sqlite3 "$data_dir/config.db" "update config set value='$USER_NAME' where key = 'remote_access_username';"
sqlite3 "$data_dir/config.db" "update config set value='$PASSWORD' where key = 'remote_access_password';"

mnemosyne "--datadir=$data_dir" --sync-server --web-server
